<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_data_pegawai extends Model
{
    //
    protected $fillable = ['nip', 'nama', 'alamat', 'jabatan'];
}
