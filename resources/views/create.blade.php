@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pegawai - create</div>
                <div class="card-body">
                    <form action="{{ url('pegawai') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <label for="">Nip</label>
                        <input type="text" class="form-control" name="nip" placeholder="Masukkan NIP"><br>
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama"><br>
                        <label for="">Alamat</label>
                        <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat"><br>
                        <label for="">Jabatan</label>
                        <select class="form-control" name="jabatan" id="">
                            <option value="Ess2">Ess2</option>
                            <option value="Ess3">Ess3</option>
                            <option value="Ess4">Ess4</option>
                            <option value="Staf">Staf</option>
                        </select>
                        <br>
                        <input type="submit" class="btn btn-success" value="Buat Pegawai">
                        <a style="float:right;" class="btn btn-danger" href="{{url('pegawai')}}">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
