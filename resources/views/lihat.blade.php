@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lihat Pegawai
                    <a style="float:right;" class="btn btn-danger" href="{{url('pegawai')}}">Kembali</a>
                </div>
                <div class="card-body">
                    <form action="{{ url('pegawai/'.$data->id) }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" style="display:block;" name="_method" value="PATCH">
                        @csrf
                        <label for="">NIP</label>
                        <input type="text" disabled class="form-control" style="display:block;" name="nip" value="{{$data->nip}}"><br>
                        <label for="">Nama</label>
                        <input type="text" disabled class="form-control" style="display:block;" name="nama" value="{{$data->nama}}"><br>
                        <label for="">Alamat</label>
                        <input type="text" disabled class="form-control" style="display:block;" name="alamat" value="{{$data->alamat}}"><br>
                        <label for="">Jabatan</label>
                        <input type="text" disabled class="form-control" style="display:block;" name="jabatan" value="{{$data->jabatan}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
