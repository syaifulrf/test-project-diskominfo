@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard
                    <a style="float:right" href="{{url('pegawai/create')}}" class="btn btn-success">Buat Pegawai</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <th>Id</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Jabatan</th>
                            <th>Action</th>
                        </thead>
                        @foreach($datas as $data)
                        <tbody>
                                <td>{{$data->id}}</td>
                                <td>{{$data->nip}}</td>
                                <td>{{$data->nama}}</td>
                                <td>{{$data->alamat}}</td>
                                <td>{{$data->jabatan}}</td>
                                <td>
                                    <a style="display:block;" class="btn btn-sm btn-success" href="{{ url('pegawai/'.$data->id) }}">lihat</a>
                                    <a style="display:block;" class="btn btn-sm btn-warning" href="{{ url('pegawai/'.$data->id.'/edit') }}">edit</a>
                                    <form action="{{ url('pegawai/'.$data->id) }}" method="POST" id="delete-form">
                                        <input type="hidden" name="_method" value="DELETE">
                                        @csrf
                                        <button style="display:block;width:100%;" type='submt' style="display:block;" class="btn btn-sm btn-danger">hapus</button>
                                    </form>
                                </td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
