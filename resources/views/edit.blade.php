@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pegawai - edit</div>
                <div class="card-body">
                    <form action="{{ url('pegawai/'.$data->id) }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PATCH">
                        @csrf
                        <label for="">Nip</label>
                        <input type="text" class="form-control" name="nip" value="{{$data->nip}}"><br>
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="nama" value="{{$data->nama}}"><br>
                        <label for="">Alamat</label>
                        <input type="text" class="form-control" name="alamat" value="{{$data->alamat}}"><br>
                        <label for="">Jabatan</label>
                        <select class="form-control" name="jabatan">
                            <option value="Ess2">Ess2</option>
                            <option value="Ess3">Ess3</option>
                            <option value="Ess4">Ess4</option>
                            <option value="Staf">Staf</option>
                        </select>
                        <br>
                        <input type="submit" class="btn btn-success" value="Update Data">
                        <a style="float:right;" class="btn btn-danger" href="{{url('pegawai')}}">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
